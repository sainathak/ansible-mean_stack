Vinyl-Proto Ansible Playbook
===================


Ansible Playbook that deploys a GCE instances and configures the MEAN stack, nginx, cutnrec for the Dev environment.

----------


Documents
-------------

- Pre-Requisities
	- sudo apt-get install python-pip python-dev build-essential git
	- sudo pip install ansible
	- sudo pip install apache-libcloud
	- sudo pip install markupsafe

- Usage
	- ansible-playbook -i hosts playbook.yml -v


